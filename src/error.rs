#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("{0}")]
    Generic(String),

    #[error("Size is expected to be {0} bytes long, {1} bytes provided")]
    SizeMismatch(usize, usize),

    #[error("Magic bytes are expected to be \"{0:X}\", \"{1:X}\" provided")]
    MagicMismatch(u32, u32),
}
