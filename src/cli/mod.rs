use std::process::exit;

use clap::{Parser, Subcommand};

mod parse_command;

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
#[command(author, version, about, long_about = None)]
#[command(disable_colored_help = false)]
#[command(
    // help_template = "{author-with-newline} {about-section}Version: {version} \n {usage-heading} {usage} \n {all-args} {tab}"
    help_template = "{usage-heading} {usage} \n\n{all-args}{tab}\n\n\x1B[1;4mAbout:\x1B[0m\n\x1B[3m  Authors: {author-with-newline}  Version: {version}\x1B[0m"
)]
struct Cli {
    /// What to do
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Clone, Debug)]
enum Command {
    /// Extract some information from a save file
    Parse(ParseCommand),

    #[cfg(debug_assertions)]
    Debug,
}

pub(crate) fn run() {
    let args = Cli::parse();

    #[cfg(debug_assertions)]
    println!("{args:#?}");

    let result = match args.command {
        Command::Parse(command) => command.run(),

        #[cfg(debug_assertions)]
        Command::Debug => debug(),
    };

    if let Err(error) = result {
        eprintln!("{error}");
        exit(1);
    }
}

#[cfg(debug_assertions)]
use pocket_parser::prelude::{Result, Error};

use self::parse_command::ParseCommand;

#[cfg(debug_assertions)]
fn debug() -> Result<()> {
    Err(Error::Generic("Nothing here".to_string()))
}
