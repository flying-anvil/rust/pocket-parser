use std::fmt::Display;

use clap::{Args, ValueEnum};
use pocket_parser::{prelude::Result, savegame::ultra_sun_moon::{SavegameMapUltraSunMoon, SavegameMapUltraSunMoonBase, player_info::{Game, TrainerIds, Gender, CountryWithSubRegion, SubRegionGermany, ConsoleRegion, Language, SpecialMoveUnlocks, BallThrowStyle}}};

#[derive(Args, Clone, Debug)]
pub struct ParseCommand {
    /// Path to the save file
    input: String,

    /// Format of the output
    #[arg(value_enum, short, long)]
    format: Option<OutputFormat>,
}

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum OutputFormat {
    #[default]
    Text,
    Json,
    Debug,
}

impl ParseCommand {
    pub fn run(&self) -> Result<()> {
        // let parsed = SavegameSunMoon::try_from_file(&self.input)?;
        // self.print_output(parsed);

        let mut savegame: SavegameMapUltraSunMoon = SavegameMapUltraSunMoon::try_from_file(&self.input)?;
        println!("\x1B[1mSavegame parsed\x1B[0m");
        self.print_output(&savegame);

        println!("\n\x1B[1mSavegame modified\x1B[0m");
        savegame.player_info_set_sid(1234);
        savegame.player_info_set_tid(567890);
        savegame.player_info_set_trainer_ids(TrainerIds::from_separate(1337, 25565));
        savegame.player_info_set_game(Game::UltraMoon);
        savegame.player_info_set_gender(Gender::Female);
        savegame.player_info_set_country_with_sub_region(CountryWithSubRegion::Germany(SubRegionGermany::MecklenburgWestPomerania));
        savegame.player_info_set_console_region(ConsoleRegion::Taiwan);
        savegame.player_info_set_language(Language::English);
        savegame.player_info_set_player_name("Lé Këtchûp (too long)");
        savegame.player_info_set_special_move_unlocks(SpecialMoveUnlocks::all());
        savegame.player_info_set_ball_throw_style(BallThrowStyle::Smug);
        self.print_output(&savegame);

        Ok(())
    }

    // fn print_output(&self, parsed: SavegameSunMoon) {
    //     match self.format.unwrap_or_default() {
    //         OutputFormat::Text => print_info_text(parsed),
    //         OutputFormat::Json => todo!("Formatting as JSON is not yet implemented"),
    //         OutputFormat::Debug => println!("{parsed:#?}"),
    //     }
    // }

    fn _print_key_value(key: &str, value: impl Display) {
        println!("{key}: \x1B[3m{value}\x1B[0m");
    }

    fn print_output(&self, savegame: &SavegameMapUltraSunMoon) {
        let ids = savegame.player_info_trainer_ids();
        let special_move_unlocks = savegame.player_info_special_move_unlocks();
        println!("SID: {}", ids.sid());
        println!("TID: {:0>6}", ids.tid());
        println!("Game: {}", savegame.player_info_game());
        println!("Gender: {}", savegame.player_info_gender());
        println!("Country: {}", savegame.player_info_country_with_sub_region());
        println!("Console Region: {}", savegame.player_info_console_region());
        println!("Language: {}", savegame.player_info_language());
        println!("Name: {}", savegame.player_info_player_name());
        println!("Mega Evolution Unlocked: {}", special_move_unlocks.mega_evolution_unlocked());
        println!("Z-Move Unlocked: {}", special_move_unlocks.z_move_unlocked());
        println!("Ball Throw Style: {}", savegame.player_info_ball_throw_style());

        // Self::print_key_value("SID", savegame.player_info_trainer_ids().sid());
        // Self::print_key_value("TID", format!("{:0>6}", savegame.player_info_trainer_ids().sid()));
    }
}

// fn print_info_text(parsed: SavegameSunMoon) {
//     println!("\x1B[1;4m{}\x1B[0m", parsed.player_info.trainer_name);
//     // println!("{}", title.long_description_string());
//     // println!();
//     // println!("Regions: {}", parsed.application_settings.region_lockout.to_string_list().join(", "));
//     // println!("EULA Version: {}", parsed.application_settings.eula_version);
//     // println!("Age restriction: {}", age_restriction);
// }
