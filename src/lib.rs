use std::char::decode_utf16;

pub mod error;
pub mod prelude;

pub mod savegame;

#[macro_export]
macro_rules! slice_offset_length {
    ($data:expr, $offset:expr, $length:expr) => {{
        let end = $offset + $length;
        &$data[$offset..end]
    }};
}

#[macro_export]
macro_rules! slice_offset_length_mut {
    ($data:expr, $offset:expr, $length:expr) => {{
        let end = $offset + $length;
        &mut $data[$offset..end]
    }};
}

pub(crate) fn read_string_utf16(slice: &[u8], offset: usize, length: usize) -> Option<String> {
    let end = offset + length * 2;
    if slice.len() < end {
        panic!("Slice is not long enough");
    }

    let sub_slice = &slice[offset..end];
    let iter = (0..length)
        .map(|i| u16::from_le_bytes([sub_slice[i * 2], sub_slice[i * 2 + 1]]));

    decode_utf16(iter).collect::<core::result::Result<String, _>>()
        .map(|value| value.trim_end_matches('\0').to_string()).ok()
}
