use std::fmt::Display;

pub mod sun_moon;
pub mod ultra_sun_moon;

pub trait SaveGameDisplay: Display {}
