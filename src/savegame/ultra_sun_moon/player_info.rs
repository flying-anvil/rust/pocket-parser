use byteorder::{ReadBytesExt, LittleEndian, WriteBytesExt};

use crate::{slice_offset_length, slice_offset_length_mut, read_string_utf16};

use super::{OFFSET_PLAYER_INFO, SavegameMapUltraSunMoon};

mod trainer_ids;
pub use trainer_ids::*;

mod game;
pub use game::*;

mod gender;
pub use gender::*;

mod region_info;
pub use region_info::*;

mod console_region;
pub use console_region::*;

mod language;
pub use language::*;

mod special_move_unlocks;
pub use special_move_unlocks::*;

mod ball_throw_style;
pub use ball_throw_style::*;

pub const OFFSET_TRAINER_IDS: usize          = 0x00;
pub const OFFSET_GAME: usize                 = 0x04;
pub const OFFSET_GENDER: usize               = 0x05;
pub const OFFSET_SUB_REGION: usize           = 0x2E;
pub const OFFSET_COUNTRY: usize              = 0x2F;
pub const OFFSET_CONSOLE_REGION: usize       = 0x34;
pub const OFFSET_LANGUAGE: usize             = 0x35;
pub const OFFSET_PLAYER_NAME: usize          = 0x38;
pub const OFFSET_SKIN_TINT: usize            = 0x54;
pub const OFFSET_SPECIAL_MOVE_UNLOCKS: usize = 0x78;
pub const OFFSET_BALL_THROW_STYLE: usize     = 0x7A;

// pub struct PlayerInfo {
// pub struct PlayerInfoOwned {
//     pub(super) raw_data_player_info: Cell<SavegameBytesUltraSunMoon>,
// }

// impl PlayerInfo {
//     pub fn trainer_ids(&self) -> TrainerIds {
//         TrainerIds(slice_offset_length![self.raw_data_player_info.get(), OFFSET_PLAYER_INFO + OFFSET_TRAINER_IDS, 0x04].read_u32::<LittleEndian>().unwrap())
//     }

//     pub fn game(&self) -> Game {
//         Game::from_byte(self.raw_data_player_info.get()[OFFSET_PLAYER_INFO + OFFSET_GAME])
//     }

//     pub fn set_game(&mut self, game: Game) {
//         self.raw_data_player_info.get_mut()[OFFSET_PLAYER_INFO + OFFSET_GAME] = game.as_byte()
//     }

//     pub fn gender(&self) -> Gender {
//         Gender::from_byte(self.raw_data_player_info.get()[OFFSET_PLAYER_INFO + OFFSET_GENDER])
//     }

//     pub fn sub_region(&self) -> Gender {
//         Gender::from_byte(self.raw_data_player_info.get()[OFFSET_PLAYER_INFO + OFFSET_GENDER])
//     }
// }

impl SavegameMapUltraSunMoon {
    pub fn player_info_trainer_ids(&self) -> TrainerIds {
        get_trainer_ids(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_trainer_ids(&mut self, new_ids: TrainerIds) {
        set_trainer_ids(&mut self.raw_data, OFFSET_PLAYER_INFO, new_ids)
    }

    pub fn player_info_set_sid(&mut self, new_sid: u16) {
        set_sid(&mut self.raw_data, OFFSET_PLAYER_INFO, new_sid)
    }

    pub fn player_info_set_tid(&mut self, new_tid: u32) {
        set_tid(&mut self.raw_data, OFFSET_PLAYER_INFO, new_tid)
    }

    // TODO: Figure out how to do this. Maybe use setters instead of &mut
    // pub fn player_info_trainer_ids_mut(&mut self) -> &mut TrainerIds {
    //     trainer_ids(&self.raw_data, OFFSET_PLAYER_INFO).borrow_mut()
    // }

    pub fn player_info_game(&self) -> Game {
        get_game(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_game(&mut self, game: Game) {
        set_game(&mut self.raw_data, OFFSET_PLAYER_INFO, game)
    }

    pub fn player_info_gender(&self) -> Gender {
        get_gender(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_gender(&mut self, new_gender: Gender) {
        set_gender(&mut self.raw_data, OFFSET_PLAYER_INFO, new_gender)
    }

    pub fn player_info_sub_region(&self) -> SubRegion {
        get_sub_region(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_sub_region(&mut self, new_sub_region: SubRegion) {
        set_sub_region(&mut self.raw_data, OFFSET_PLAYER_INFO, new_sub_region)
    }

    pub fn player_info_country(&self) -> Country {
        get_country(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_country(&mut self, new_country: Country) {
        set_country(&mut self.raw_data, OFFSET_PLAYER_INFO, new_country)
    }

    pub fn player_info_country_with_sub_region(&self) -> CountryWithSubRegion {
        get_country_with_sub_region(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_country_with_sub_region(&mut self, new_value: CountryWithSubRegion) {
        set_country_with_sub_region(&mut self.raw_data, OFFSET_PLAYER_INFO, new_value)
    }

    pub fn player_info_console_region(&self) -> ConsoleRegion {
        get_console_region(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_console_region(&mut self, new_console_region: ConsoleRegion) {
        set_console_region(&mut self.raw_data, OFFSET_PLAYER_INFO, new_console_region)
    }

    pub fn player_info_language(&self) -> Language {
        get_language(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_language(&mut self, new_language: Language) {
        set_language(&mut self.raw_data, OFFSET_PLAYER_INFO, new_language)
    }

    pub fn player_info_player_name(&self) -> String {
        get_player_name(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_player_name(&mut self, new_player_name: &str) {
        set_player_name(&mut self.raw_data, OFFSET_PLAYER_INFO, new_player_name)
    }

    pub fn player_info_special_move_unlocks(&self) -> SpecialMoveUnlocks {
        get_special_move_unlocks(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_special_move_unlocks(&mut self, new_value: SpecialMoveUnlocks) {
        set_special_move_unlocks(&mut self.raw_data, OFFSET_PLAYER_INFO, new_value)
    }

    pub fn player_info_ball_throw_style(&self) -> BallThrowStyle {
        get_ball_throw_style(&self.raw_data, OFFSET_PLAYER_INFO)
    }

    pub fn player_info_set_ball_throw_style(&mut self, new_style: BallThrowStyle) {
        set_ball_throw_style(&mut self.raw_data, OFFSET_PLAYER_INFO, new_style)
    }
}

// The following functions are its own things to later be able to also read/write data from/tto a separated PlayerInfo struct.

fn get_trainer_ids(bytes: &[u8], base_offset: usize) -> TrainerIds {
    TrainerIds::from_combined(slice_offset_length![bytes, base_offset + OFFSET_TRAINER_IDS, 0x04].read_u32::<LittleEndian>().unwrap())
}

fn set_trainer_ids(bytes: &mut [u8], base_offset: usize, ids: TrainerIds) {
    slice_offset_length_mut![bytes, base_offset + OFFSET_TRAINER_IDS, 0x04].write_u32::<LittleEndian>(ids.as_combined()).unwrap();
}

fn set_sid(bytes: &mut [u8], base_offset: usize, new_sid: u16) {
    let tid = get_trainer_ids(bytes, base_offset).tid();
    let new_ids = TrainerIds::from_separate(new_sid, tid);

    slice_offset_length_mut![bytes, base_offset + OFFSET_TRAINER_IDS, 0x04].write_u32::<LittleEndian>(new_ids.as_combined()).unwrap();
}

fn set_tid(bytes: &mut [u8], base_offset: usize, new_tid: u32) {
    let sid = get_trainer_ids(bytes, base_offset).sid();
    let new_ids = TrainerIds::from_separate(sid, new_tid);

    slice_offset_length_mut![bytes, base_offset + OFFSET_TRAINER_IDS, 0x04].write_u32::<LittleEndian>(new_ids.as_combined()).unwrap();
}

fn get_game(bytes: &[u8], base_offset: usize) -> Game {
    Game::from_byte(bytes[base_offset + OFFSET_GAME])
}

fn set_game(bytes: &mut [u8], base_offset: usize, game: Game) {
    bytes[base_offset + OFFSET_GAME] = game.as_byte()
}

fn get_gender(bytes: &[u8], base_offset: usize) -> Gender {
    Gender::from_byte(bytes[base_offset + OFFSET_GENDER])
}

fn set_gender(bytes: &mut [u8], base_offset: usize, new_gender: Gender) {
    bytes[base_offset + OFFSET_GENDER] = new_gender.as_byte()
}

fn get_sub_region(bytes: &[u8], base_offset: usize) -> SubRegion {
    SubRegion(bytes[base_offset + OFFSET_SUB_REGION])
}

fn set_sub_region(bytes: &mut [u8], base_offset: usize, new_sub_region: SubRegion) {
    bytes[base_offset + OFFSET_SUB_REGION] = new_sub_region.as_byte()
}

fn get_country(bytes: &[u8], base_offset: usize) -> Country {
    Country::from_byte(bytes[base_offset + OFFSET_COUNTRY])
}

fn set_country(bytes: &mut [u8], base_offset: usize, new_country: Country) {
    bytes[base_offset + OFFSET_COUNTRY] = new_country.as_byte()
}

fn get_country_with_sub_region(bytes: &[u8], base_offset: usize) -> CountryWithSubRegion {
    CountryWithSubRegion::from_parts(
        get_country(bytes, base_offset),
        get_sub_region(bytes, base_offset),
    )
}

fn set_country_with_sub_region(bytes: &mut [u8], base_offset: usize, new_value: CountryWithSubRegion) {
    set_sub_region(bytes, base_offset, new_value.sub_region());
    set_country(bytes, base_offset, new_value.country());
}

fn get_console_region(bytes: &[u8], base_offset: usize) -> ConsoleRegion {
    ConsoleRegion::from_byte(bytes[base_offset + OFFSET_CONSOLE_REGION])
}

fn set_console_region(bytes: &mut [u8], base_offset: usize, new_console_region: ConsoleRegion) {
    bytes[base_offset + OFFSET_CONSOLE_REGION] = new_console_region.as_byte()
}

fn get_language(bytes: &[u8], base_offset: usize) -> Language {
    Language::from_byte(bytes[base_offset + OFFSET_LANGUAGE])
}

fn set_language(bytes: &mut [u8], base_offset: usize, new_language: Language) {
    bytes[base_offset + OFFSET_LANGUAGE] = new_language.as_byte()
}

fn get_player_name(bytes: &[u8], base_offset: usize) -> String {
    read_string_utf16(bytes, base_offset + OFFSET_PLAYER_NAME, 0x0C).unwrap()
}

fn set_player_name(bytes: &mut [u8], base_offset: usize, new_player_name: &str) {
    let mut chars = new_player_name.encode_utf16().collect::<Vec<u16>>();
    chars.resize(0x0C, 0);

    chars.iter().enumerate().for_each(|(i, char)| {
        let char_offset = base_offset + OFFSET_PLAYER_NAME + (i * 2);
        slice_offset_length_mut!(bytes, char_offset, 2).write_u16::<LittleEndian>(*char).unwrap();
    });
}

fn get_special_move_unlocks(bytes: &[u8], base_offset: usize) -> SpecialMoveUnlocks {
    SpecialMoveUnlocks::from_byte(bytes[base_offset + OFFSET_SPECIAL_MOVE_UNLOCKS])
}

fn set_special_move_unlocks(bytes: &mut [u8], base_offset: usize, new_value: SpecialMoveUnlocks) {
    bytes[base_offset + OFFSET_SPECIAL_MOVE_UNLOCKS] = new_value.as_byte()
}

fn get_ball_throw_style(bytes: &[u8], base_offset: usize) -> BallThrowStyle {
    BallThrowStyle::from_byte(bytes[base_offset + OFFSET_BALL_THROW_STYLE])
}

fn set_ball_throw_style(bytes: &mut [u8], base_offset: usize, new_style: BallThrowStyle) {
    bytes[base_offset + OFFSET_BALL_THROW_STYLE] = new_style.as_byte()
}
