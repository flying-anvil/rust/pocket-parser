#[derive(Clone, Copy, Debug)]
pub struct TrainerIds(u32);

impl TrainerIds {
    pub fn from_combined(ids: u32) -> Self {
        Self(ids)
    }

    pub fn from_separate(sid: u16, tid: u32) -> Self {
        if sid > 4294 {
            panic!("Trainer SID must not be higher than 4294 ({sid} given)");
        }

        if tid > 999999 {
            panic!("Trainer TID must not be higher than 999999 ({tid} given)");
        }

        let combined = (sid as u32 * 1000000) + tid;
        Self(combined)
    }

    /// Secret ID is the first 4 digits of the combined raw ID.
    /// It can range from 0000 to 4294.
    pub fn sid(&self) -> u16 {
        let tid = self.0 % 1000000;
        ((self.0 - tid) / 1000000) as u16
    }

    /// Trainer ID is the last 6 digits of the combined raw ID
    /// It can range from 000000 to 999999.
    pub fn tid(&self) -> u32 {
        self.0 % 1000000
    }

    pub fn as_combined(&self) -> u32 {
        self.0
    }
}
