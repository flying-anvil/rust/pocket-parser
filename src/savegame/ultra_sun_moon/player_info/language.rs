use std::fmt::Display;

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum Language {
    // None            = 0x00, // Invalid // Not sure why JPN starts at 0x01
    Japanese           = 0x01,
    English            = 0x02,
    French             = 0x03,
    Italian            = 0x04,
    German             = 0x05,
    Unused             = 0x06, // Maybe this was intended for a dialect/variant of German or Spanish?
    Spanish            = 0x07,
    Korean             = 0x08,
    ChineseSimplified  = 0x09,
    ChineseTraditional = 0x0A,

    Unknown(u8),
}


impl Language {
    pub fn from_byte(byte: u8) -> Self {
        match byte {
            1  => Self::Japanese,
            2  => Self::English,
            3  => Self::French,
            4  => Self::Italian,
            5  => Self::German,
            6  => Self::Unused,
            7  => Self::Spanish,
            8  => Self::Korean,
            9  => Self::ChineseSimplified,
            10 => Self::ChineseTraditional,

            other => Self::Unknown(other),
        }
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            Self::Japanese => 1,
            Self::English  => 2,
            Self::French   => 3,
            Self::Italian  => 4,
            Self::German   => 5,
            Self::Unused   => 6,
            Self::Spanish  => 7,
            Self::Korean   => 8,
            Self::ChineseSimplified  => 9,
            Self::ChineseTraditional => 10,

            Self::Unknown(raw) => *raw,
        }
    }
}

impl Display for Language {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Language::Japanese => "Japanese",
            Language::English  => "English",
            Language::French   => "French",
            Language::Italian  => "Italian",
            Language::German   => "German",
            Language::Unused   => "Unused",
            Language::Spanish  => "Spanish",
            Language::Korean   => "Korean",
            Language::ChineseSimplified  => "ChineseSimplified",
            Language::ChineseTraditional => "ChineseTraditional",
            Language::Unknown(_) => "Unknown",
        })
    }
}
