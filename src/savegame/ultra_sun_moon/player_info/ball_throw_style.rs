use std::fmt::Display;

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum BallThrowStyle {
    Normal     = 0x00,
    Elegant    = 0x01,
    Girlish    = 0x02,
    Reverent   = 0x03,
    Smug       = 0x04,
    LeftHanded = 0x05,
    Passionate = 0x06,
    Idol       = 0x07,
    Nihilist   = 0x08,

    Unknown(u8),
}

impl BallThrowStyle {
    pub fn from_byte(byte: u8) -> Self {
        match byte {
            0 => Self::Normal,
            1 => Self::Elegant,
            2 => Self::Girlish,
            3 => Self::Reverent,
            4 => Self::Smug,
            5 => Self::LeftHanded,
            6 => Self::Passionate,
            7 => Self::Idol,
            8 => Self::Nihilist,

            other => Self::Unknown(other),
        }
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            Self::Normal     => 0,
            Self::Elegant    => 1,
            Self::Girlish    => 2,
            Self::Reverent   => 3,
            Self::Smug       => 4,
            Self::LeftHanded => 5,
            Self::Passionate => 6,
            Self::Idol       => 7,
            Self::Nihilist   => 8,

            Self::Unknown(raw) => *raw,
        }
    }
}

impl Display for BallThrowStyle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            BallThrowStyle::Normal     => "Normal",
            BallThrowStyle::Elegant    => "Elegant",
            BallThrowStyle::Girlish    => "Girlish",
            BallThrowStyle::Reverent   => "Reverent",
            BallThrowStyle::Smug       => "Smug",
            BallThrowStyle::LeftHanded => "LeftHanded",
            BallThrowStyle::Passionate => "Passionate",
            BallThrowStyle::Idol       => "Idol",
            BallThrowStyle::Nihilist   => "Nihilist",
            BallThrowStyle::Unknown(_) => "Unknown",
        })
    }
}
