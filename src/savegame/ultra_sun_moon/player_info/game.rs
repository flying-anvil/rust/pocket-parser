use std::fmt::Display;

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum Game {
    // TODO: Is this a common thing that games from other generations also have, filling the other values?
    Sun = 30,
    Moon = 31,
    UltraSun = 32,
    UltraMoon = 33,

    Unknown(u8),
}

impl Game {
    pub fn from_byte(byte: u8) -> Self {
        match byte {
            30 => Self::Sun,
            31 => Self::Moon,
            32 => Self::UltraSun,
            33 => Self::UltraMoon,

            other => Self::Unknown(other),
        }
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            Self::Sun       => 30,
            Self::Moon      => 31,
            Self::UltraSun  => 32,
            Self::UltraMoon => 33,

            Self::Unknown(raw) => *raw,
        }
    }
}

impl Display for Game {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Game::Sun        => "Sun",
            Game::Moon       => "Moon",
            Game::UltraSun   => "Ultra Sun",
            Game::UltraMoon  => "Ultra Moon",
            Game::Unknown(_) => "Unknown",
        })
    }
}
