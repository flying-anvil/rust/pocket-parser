use std::fmt::Display;

#[repr(u8)]
pub enum Gender {
    Male   = 0,
    Female = 1,

    Unknown(u8),
}

impl Gender {
    pub fn from_byte(byte: u8) -> Self {
        match byte {
            0 => Self::Male,
            1 => Self::Female,

            other => Self::Unknown(other),
        }
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            Self::Male   => 0,
            Self::Female => 1,

            Self::Unknown(raw) => *raw,
        }
    }
}

impl Display for Gender {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Gender::Male       => "Male",
            Gender::Female     => "Female",
            Gender::Unknown(_) => "Unknown",
        })
    }
}
