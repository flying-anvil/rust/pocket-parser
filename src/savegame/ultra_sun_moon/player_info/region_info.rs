use std::fmt::Display;

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum Country {
    Albania  = 0x40,
    Germany  = 0x4E,
    Norway   = 0x60,
    Zimbabwe = 0x70,
    Andorra  = 0x7A,

    Unknown(u8),
}

impl Country {
    pub fn from_byte(byte: u8) -> Self {
        match byte {
            0x40 => Self::Albania,
            0x4E => Self::Germany,
            0x60 => Self::Norway,
            0x70 => Self::Zimbabwe,
            0x7A => Self::Andorra,

            other => Self::Unknown(other),
        }
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            Country::Albania  => 0x40,
            Country::Germany  => 0x4E,
            Country::Norway   => 0x60,
            Country::Zimbabwe => 0x70,
            Country::Andorra  => 0x7A,

            Country::Unknown(other) => *other,
        }
    }
}

impl Display for Country {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Country::Albania  => "Albania",
            Country::Germany  => "Germany",
            Country::Norway   => "Norway",
            Country::Zimbabwe => "Zimbabwe",
            Country::Andorra  => "Andorra",

            Country::Unknown(_) => "Unknown",
        })
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum CountryWithSubRegion {
    Albania(u8)               = 0x40,
    Germany(SubRegionGermany) = 0x4E,
    Norway(u8)                = 0x60,
    Zimbabwe(u8)              = 0x70,
    Andorra(u8)               = 0x7A,

    Unknown(u8, u8),
}
impl CountryWithSubRegion {
    pub fn from_parts(country: Country, sub_region: SubRegion) -> CountryWithSubRegion {
        match country {
            Country::Albania  => CountryWithSubRegion::Albania(sub_region.into()),
            Country::Germany  => CountryWithSubRegion::Germany(sub_region.into()),
            Country::Norway   => CountryWithSubRegion::Norway(sub_region.into()),
            Country::Zimbabwe => CountryWithSubRegion::Zimbabwe(sub_region.into()),
            Country::Andorra  => CountryWithSubRegion::Andorra(sub_region.into()),

            other => Self::Unknown(other.as_byte(), sub_region.into()),
        }
    }

    pub fn country(&self) -> Country {
        match self {
            CountryWithSubRegion::Albania(_)  => Country::Albania,
            CountryWithSubRegion::Germany(_)  => Country::Germany,
            CountryWithSubRegion::Norway(_)   => Country::Norway,
            CountryWithSubRegion::Zimbabwe(_) => Country::Zimbabwe,
            CountryWithSubRegion::Andorra(_)  => Country::Andorra,

            CountryWithSubRegion::Unknown(other, _) => Country::Unknown(*other),
        }
    }

    pub fn sub_region(&self) -> SubRegion {
        SubRegion(match *self {
            CountryWithSubRegion::Albania(sub) => sub,
            CountryWithSubRegion::Germany(sub) => sub.into(),
            CountryWithSubRegion::Norway(sub) => sub,
            CountryWithSubRegion::Zimbabwe(sub) => sub,
            CountryWithSubRegion::Andorra(sub) => sub,

            CountryWithSubRegion::Unknown(_, other) => other,
        })
    }
}

pub struct SubRegion(pub u8);

impl SubRegion {
    pub fn as_byte(&self) -> u8 {
        self.0
    }
}

impl From<SubRegion> for u8 {
    fn from(value: SubRegion) -> Self {
        value.as_byte()
    }
}

impl Display for CountryWithSubRegion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            // TODO: Complete
            CountryWithSubRegion::Albania(_sub) => write!(f, "Albania"),
            CountryWithSubRegion::Germany(sub) => write!(f, "Germany ({sub})"),
            CountryWithSubRegion::Norway(_sub) => write!(f, "Norway"),
            CountryWithSubRegion::Zimbabwe(_sub) => write!(f, "Zimbabwe"),
            CountryWithSubRegion::Andorra(_sub) => write!(f, "Andorra"),

            CountryWithSubRegion::Unknown(_, _) => write!(f, "Unknown"), // It's so much…
        }
    }
}

impl From<CountryWithSubRegion> for Country {
    fn from(value: CountryWithSubRegion) -> Self {
        value.country()
    }
}

impl From<CountryWithSubRegion> for SubRegion {
    fn from(value: CountryWithSubRegion) -> Self {
        value.sub_region()
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum SubRegionGermany {
    Berlin                   = 0x02,
    Hesse                    = 0x03,
    BadenWurttemberg         = 0x04,
    Bavaria                  = 0x05,
    Brandenburg              = 0x06,
    Bremen                   = 0x07,
    Hamburg                  = 0x08,
    MecklenburgWestPomerania = 0x09, // WTF english language…
    LowerSaxony              = 0x0A,
    NorthRhineWestphalia     = 0x0B,
    RhinelandPalatinate      = 0x0C,
    Saarland                 = 0x0D,
    Saxony                   = 0x0E,
    SaxonyAnhalt             = 0x0F,
    SchleswigHolstein        = 0x10,
    Thuringia                = 0x11,

    Unknown(u8),
}

impl Display for SubRegionGermany {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            SubRegionGermany::Berlin                   => "Berlin",
            SubRegionGermany::Hesse                    => "Hesse",
            SubRegionGermany::BadenWurttemberg         => "Baden-Wurttemberg",
            SubRegionGermany::Bavaria                  => "Bavaria",
            SubRegionGermany::Brandenburg              => "Brandenburg",
            SubRegionGermany::Bremen                   => "Bremen",
            SubRegionGermany::Hamburg                  => "Hamburg",
            SubRegionGermany::MecklenburgWestPomerania => "Mecklenburg-West Pomerania",
            SubRegionGermany::LowerSaxony              => "Lower Saxony",
            SubRegionGermany::NorthRhineWestphalia     => "North Rhine-Westphalia",
            SubRegionGermany::RhinelandPalatinate      => "Rhineland-Palatinate",
            SubRegionGermany::Saarland                 => "Saarland",
            SubRegionGermany::Saxony                   => "Saxony",
            SubRegionGermany::SaxonyAnhalt             => "Saxony-Anhalt",
            SubRegionGermany::SchleswigHolstein        => "Schleswig-Holstein",
            SubRegionGermany::Thuringia                => "Thuringia",

            SubRegionGermany::Unknown(_) => "Unknown"
        })
    }
}

impl From<SubRegion> for SubRegionGermany {
    fn from(value: SubRegion) -> Self {
        match value.0 {
            0x02 => SubRegionGermany::Berlin,
            0x03 => SubRegionGermany::Hesse,
            0x04 => SubRegionGermany::BadenWurttemberg,
            0x05 => SubRegionGermany::Bavaria,
            0x06 => SubRegionGermany::Brandenburg,
            0x07 => SubRegionGermany::Bremen,
            0x08 => SubRegionGermany::Hamburg,
            0x09 => SubRegionGermany::MecklenburgWestPomerania,
            0x0A => SubRegionGermany::LowerSaxony,
            0x0B => SubRegionGermany::NorthRhineWestphalia,
            0x0C => SubRegionGermany::RhinelandPalatinate,
            0x0D => SubRegionGermany::Saarland,
            0x0E => SubRegionGermany::Saxony,
            0x0F => SubRegionGermany::SaxonyAnhalt,
            0x10 => SubRegionGermany::SchleswigHolstein,
            0x11 => SubRegionGermany::Thuringia,

            other => SubRegionGermany::Unknown(other),
        }
    }
}

impl From<SubRegionGermany> for u8 {
    fn from(value: SubRegionGermany) -> Self {
        match value {
            SubRegionGermany::Berlin                   => 0x02,
            SubRegionGermany::Hesse                    => 0x03,
            SubRegionGermany::BadenWurttemberg         => 0x04,
            SubRegionGermany::Bavaria                  => 0x05,
            SubRegionGermany::Brandenburg              => 0x06,
            SubRegionGermany::Bremen                   => 0x07,
            SubRegionGermany::Hamburg                  => 0x08,
            SubRegionGermany::MecklenburgWestPomerania => 0x09,
            SubRegionGermany::LowerSaxony              => 0x0A,
            SubRegionGermany::NorthRhineWestphalia     => 0x0B,
            SubRegionGermany::RhinelandPalatinate      => 0x0C,
            SubRegionGermany::Saarland                 => 0x0D,
            SubRegionGermany::Saxony                   => 0x0E,
            SubRegionGermany::SaxonyAnhalt             => 0x0F,
            SubRegionGermany::SchleswigHolstein        => 0x10,
            SubRegionGermany::Thuringia                => 0x11,

            SubRegionGermany::Unknown(other) => other,
        }
    }
}
