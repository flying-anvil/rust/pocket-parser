use bitflags::bitflags;

bitflags! {
    #[derive(Default, Clone, Copy, Debug, PartialEq, Eq, Hash,)]
    pub struct SpecialMoveUnlocks: u8 {
        const MegaEvolution = 0b0000_0001;
        const ZMove         = 0b0000_0010;

        const None = 0b0000_0000;
        const Both = 0b0000_0011;
    }
}

impl SpecialMoveUnlocks {
    pub fn from_byte(byte: u8) -> Self {
        Self::from_bits_retain(byte)
    }

    pub fn as_byte(&self) -> u8 {
        self.bits()
    }

    pub fn mega_evolution_unlocked(&self) -> bool {
        self.intersects(Self::MegaEvolution)
    }

    pub fn z_move_unlocked(&self) -> bool {
        self.intersects(Self::ZMove)
    }

    pub fn set_mega_evolution_unlocked(&mut self, value: bool) {
        self.set(Self::MegaEvolution, value);
    }

    pub fn set_z_move_unlocked(&mut self, value: bool) {
        self.set(Self::ZMove, value);
    }

    pub fn set_none(&mut self) {
        self.remove(Self::Both)
    }

    pub fn set_both(&mut self) {
        self.insert(Self::Both)
    }
}
