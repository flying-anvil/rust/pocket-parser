use std::fmt::Display;

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum ConsoleRegion {
    Japan   = 0x00,
    America = 0x01, // North and South America
    Europe  = 0x02, // Europe and Australia
    Unused  = 0x03, // Maybe Australia was planned to be its own region at some point?
    China   = 0x04,
    Korea   = 0x05,
    Taiwan  = 0x06,

    Unknown(u8),
}

impl ConsoleRegion {
    pub fn from_byte(byte: u8) -> Self {
        match byte {
            0 => Self::Japan,
            1 => Self::America,
            2 => Self::Europe,
            3 => Self::Unused,
            4 => Self::China,
            5 => Self::Korea,
            6 => Self::Taiwan,

            other => Self::Unknown(other),
        }
    }

    pub fn as_byte(&self) -> u8 {
        match self {
            Self::Japan   => 0,
            Self::America => 1,
            Self::Europe  => 2,
            Self::Unused  => 3,
            Self::China   => 4,
            Self::Korea   => 5,
            Self::Taiwan  => 6,

            Self::Unknown(raw) => *raw,
        }
    }
}

impl Display for ConsoleRegion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            ConsoleRegion::Japan      => "Japan",
            ConsoleRegion::America    => "America",
            ConsoleRegion::Europe     => "Europe",
            ConsoleRegion::Unused     => "Unused",
            ConsoleRegion::China      => "China",
            ConsoleRegion::Korea      => "Korea",
            ConsoleRegion::Taiwan     => "Taiwan",
            ConsoleRegion::Unknown(_) => "Unknown",
        })
    }
}
