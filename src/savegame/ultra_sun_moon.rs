use std::path::Path;

use crate::prelude::{Result, Error};

pub const SAVE_FILE_SIZE: usize = 445440;
pub const CHUNK_SIZE: usize = 0x200;

pub const OFFSET_PLAYER_INFO: usize = 0x1400;

pub mod player_info;

pub type SavegameBytesUltraSunMoon = [u8; SAVE_FILE_SIZE];

#[derive(Debug)]
pub struct SavegameMapUltraSunMoon {
    raw_data: SavegameBytesUltraSunMoon,

    // raw_data_player_info: &'a [u8; chunks_to_bytes(2)],
}

// #[derive(Debug)]
// pub struct SavegameMapUltraSunMoonMut {
//     raw_data: SavegameBytesUltraSunMoon,
// }

pub trait SavegameMapUltraSunMoonBase {
    fn try_from_file(path: impl AsRef<Path>) -> Result<SavegameMapUltraSunMoon> {
        let bytes = std::fs::read(path).map_err(|error| Error::Generic(format!("Could not open file: {error}")))?;
        Self::try_from_bytes(&bytes)
    }

    fn try_from_bytes(bytes: &[u8]) -> Result<SavegameMapUltraSunMoon> {
        if bytes.len() != SAVE_FILE_SIZE {
            return Err(Error::SizeMismatch(SAVE_FILE_SIZE, bytes.len()));
        }

        let raw_data = bytes.try_into().unwrap();
        Ok(SavegameMapUltraSunMoon {
            raw_data,
            // raw_data_player_info: array_ref![raw_data, 0x1400, chunks_to_bytes(2)],
            // raw_data_player_info: &raw_data[range_creator(0x1400, 2)],
        })
    }
}

impl SavegameMapUltraSunMoonBase for SavegameMapUltraSunMoon {}
// impl SavegameMapUltraSunMoonBase for SavegameMapUltraSunMoonMut {}

impl SavegameMapUltraSunMoon {
    pub fn into_bytes(self) -> SavegameBytesUltraSunMoon {
        self.raw_data
    }
    // pub fn player_info(&self) -> PlayerInfo {
    //     PlayerInfo { raw_data_player_info: Cell::new(self.raw_data) }
    //     // PlayerInfo { raw_data_player_info: array_ref![self.raw_data, 0x1400, chunks_to_bytes(2)] }
    // }

    // pub fn player_info_mut(&mut self) -> PlayerInfo {
    //     PlayerInfo { raw_data_player_info: &mut self.raw_data }
    //     // PlayerInfo { raw_data_player_info: array_ref![self.raw_data, 0x1400, chunks_to_bytes(2)] }
    // }
}

// const fn chunks_to_bytes(chunks: usize) -> usize {
//     chunks * CHUNK_SIZE
// }

// const fn chunk_range(offset: usize, chunks: usize) -> Range<usize> {
//     let end = offset + chunks_to_bytes(chunks);
//     offset..end
// }
