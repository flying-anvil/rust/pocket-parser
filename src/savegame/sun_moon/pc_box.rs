use std::char::decode_utf16;

use crate::prelude::Result;

#[derive(Debug)]
pub struct PcBoxes {
    pub unlocked: u8,
    pub current_box: u8,
    pub flags: u8,
    // pub boxes: [PcBox; 32],
    pub boxes: Vec<PcBox>,
    pub battle_teams: Vec<BattleTeam>,
}

#[derive(Debug)]
pub struct PcBox {
    name: String,
    background: Background,
    // pokemon: [_; 30?];
}

#[derive(Default, Debug)]
#[repr(u8)]
enum Background {
    #[default]
    Forest = 0,
    City = 1,
    Desert = 2,
    Savanna = 3,

    Crag = 4,
    Volcano = 5,
    Snow = 6,
    Case = 7,

    Beach = 8,
    Seafloor = 9,
    River = 10,
    Sky = 11,

    PokeCenter = 12,
    Machine = 13,
    Checks = 14,
    Simple = 15,
}

#[derive(Debug)]
pub struct BattleTeam {
    name: String,
}

const BOX_NAME_SIZE: usize = 34;  // bytes, meaning 17 UTF-16 chars
const TEAM_NAME_SIZE: usize = 22; // bytes, meaning 11 UTF-16 chars

const OFFSET_BOX_NAMES: usize = 0;
const OFFSET_TEAM_NAMES: usize = 0x440;
const OFFSET_BACKGROUNDS: usize = 0x5C0;
const OFFSET_FLAGS: usize = 0x5E0;
const OFFSET_UNLOCKED: usize = 0x5E1;
const OFFSET_CURRENT_BOX: usize = 0x5E3;

impl PcBoxes {
    pub fn parse_box_data(data: &[u8]) -> Result<PcBoxes> {
        // let mut reader = Cursor::new(data);

        // let mut boxes: Vec<PcBox> = Vec::with_capacity(32);
        // let mut name_buffer = [0u8; 34];
        // for _ in 0..32 {
        //     reader.read_exact(&mut name_buffer).unwrap();

        //     let (_, chars, _) = unsafe {
        //         name_buffer.align_to::<u16>()
        //     };

        //     let box_name = String::from_utf16_lossy(chars).trim_end_matches('\0').to_owned();
        //     boxes.push(PcBox::empty_with_name(&box_name));
        // }

        // let mut battle_teams = Vec::with_capacity(6);
        // let mut name_buffer = vec![0u8; 22];
        // for _ in 0..6 {
        //     reader.read_exact(&mut name_buffer).unwrap();

        //     let (_, chars, _) = unsafe {
        //         name_buffer.align_to::<u16>()
        //     };

        //     let team_name = String::from_utf16_lossy(chars).trim_end_matches('\0').to_owned();
        //     battle_teams.push(team_name);
        // }

        let mut boxes = Vec::with_capacity(32);
        for i in 0..32 {
            boxes.push(PcBox::parse_single_box(data, i));
        }

        let mut battle_teams = Vec::with_capacity(6);
        for i in 0..6 {
            battle_teams.push(BattleTeam::parse_single_team(data, i));
        }

        Ok(PcBoxes {
            unlocked: Self::read_unlocked_box_count(data),
            current_box: Self::read_current_box(data),
            flags: Self::read_flags(data),
            boxes,
            battle_teams: battle_teams,
        })
    }

    pub fn read_flags(data: &[u8]) -> u8 {
        data[OFFSET_FLAGS]
    }

    pub fn read_unlocked_box_count(data: &[u8]) -> u8 {
        data[OFFSET_UNLOCKED]
    }

    pub fn read_current_box(data: &[u8]) -> u8 {
        data[OFFSET_CURRENT_BOX]
    }
}

impl PcBox {
    pub fn empty_with_name(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            background: Background::default(),
        }
    }

    /// `data` is the whole PC chunk with information about all PC data, not only a single box.
    pub fn parse_single_box(data: &[u8], box_index: u8) -> Self {
        if box_index > 31 {
            panic!("Box is out of bounds! There are only 32 boxes (highest index is 31)");
        }

        let uindex = box_index as usize;

        let name_offset = OFFSET_BOX_NAMES + BOX_NAME_SIZE * uindex;
        let box_name = read_string_utf16(data, name_offset, BOX_NAME_SIZE / 2).unwrap_or("".to_string());

        let background_offset = OFFSET_BACKGROUNDS + uindex;
        let background = Background::from(data[background_offset]);

        PcBox {
            name: box_name,
            background,
        }
    }
}

impl BattleTeam {
    fn parse_single_team(data: &[u8], index: u8) -> Self {
        if index > 5 {
            panic!("Team is out of bounds! There are only 6 teams (highest index is 5)");
        }

        let name_offset = OFFSET_TEAM_NAMES + TEAM_NAME_SIZE * index as usize;
        let team_name = read_string_utf16(data, name_offset, TEAM_NAME_SIZE / 2).unwrap_or("".to_string());

        Self {
            name: team_name,
        }
    }
}

fn read_string_utf16(slice: &[u8], offset: usize, length: usize) -> Option<String> {
    let end = offset + length * 2;
    if slice.len() < end {
        panic!("Slice is not long enough");
    }

    let sub_slice = &slice[offset..end];

    // let (_, chars, _) = unsafe {
    //     sub_slice.align_to::<u16>()
    // };

    // String::from_utf16(chars).ok()

    let iter = (0..length)
        .map(|i| u16::from_le_bytes([sub_slice[i * 2], sub_slice[i * 2 + 1]]));

    decode_utf16(iter).collect::<core::result::Result<String, _>>()
        .map(|value| value.trim_end_matches('\0').to_string()).ok()
}

impl From<u8> for Background {
    fn from(value: u8) -> Self {
        if value > 15 {
            panic!("Invalid background ID {value}");
        }

        unsafe {std::mem::transmute(value)}
    }
}
