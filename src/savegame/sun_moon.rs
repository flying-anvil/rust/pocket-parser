#![allow(unused)]
#![warn(unused_imports)]
// The information here is mostly incorrect,
// as it's based on a savegame from USUM, which is slightly different.
#![allow(clippy::all)]

use std::path::Path;

use crate::prelude::{Result, Error};

use self::pc_box::PcBoxes;

mod pc_box;

#[derive(Debug)]
pub struct SavegameSunMoon {
    pub player_info: PlayerInfo,
    pub pc_boxes: PcBoxes,
}

#[derive(Debug)]
pub struct PlayerInfo {
    pub trainer_name: String,
    pub playtime: u16,
}

pub const SAVE_FILE_SIZE: usize = 0x6CC00 /* 0x6BE00 for regular sun/moon */;

pub const OFFSET_BOX_DATA: usize = 0x4800;
pub const LENGTH_BOX_DATA: usize = 0x0600; // Might be smaller (like 0x5E2) due to unused/padding bytes

impl SavegameSunMoon {
    pub fn try_from_file(path: impl AsRef<Path>) -> Result<Self> {
        let bytes = std::fs::read(path).map_err(|error| Error::Generic(format!("Could not open file: {error}")))?;
        Self::try_from_bytes(&bytes)
    }

    pub fn try_from_bytes(bytes: &[u8]) -> Result<Self> {
        if bytes.len() != SAVE_FILE_SIZE {
            return Err(Error::SizeMismatch(SAVE_FILE_SIZE, bytes.len()));
        }

        let partition_data = &bytes[0x0400..];
        Self::parse_data_partition(partition_data)
    }

    fn parse_data_partition(partition_data: &[u8]) -> Result<Self> {
        let box_data = &partition_data[OFFSET_BOX_DATA..OFFSET_BOX_DATA + LENGTH_BOX_DATA];

        // let box1 = PcBox::parse_single_box(box_data, 10);
        // println!("{box1:#?}");
        // todo!();

        Ok(Self {
            player_info: Self::parse_player_info(&partition_data[..])?,
            pc_boxes: PcBoxes::parse_box_data(box_data)?,
        })

    }

    fn parse_player_info(_data: &[u8]) -> Result<PlayerInfo> {
        Ok(PlayerInfo {
            trainer_name: "TODO".to_string(),
            playtime: Default::default(),
        })
    }

}
