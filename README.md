# Pocket Parser

Parser for Pokémon savegames.

# Features

- [ ] Detect the generation/game
- [ ] Games
  - [ ] Sun/Moon
    - [ ] Player Info
      - [ ] Trainer Name
      - [ ] Playtime
    - [ ] Box Info
      - [ ] Box Names
      - [ ] Number of Pokémon
    - [ ] Pokédex
